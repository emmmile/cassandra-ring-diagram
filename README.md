# cassandra-ring-diagram

This repo contains the source code for generating this animation:

![cstar running on a Cassandra cluster](./image.gif)

Featured in this blog post:

https://labs.spotify.com/2018/09/04/introducing-cstar-the-spotify-cassandra-orchestration-tool-now-open-source/

## How to use

Just clone the repo and do:

```bash
make
```

This generates a gif animation. You can tune the output changing the `Makefile` and the `cassandra-ring.tex` file.
The code is pretty well documented so should be fairly easy to change.

## Dependencies

The image is written in LaTeX, using the `tikz` drawing library. The compiled output (a PDF) is then converted to
image file using `imagemagick`.

## Similar work

This work is based on the example available on texample, ["a simple cycle"](http://www.texample.net/tikz/examples/cycle/),
by Jerome Tremblay.
